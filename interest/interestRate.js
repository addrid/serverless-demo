
"use strict"

var exports = module.exports = function(name) {
    const interestRate = 1.11;
    const intereString = "interestRate";
    
    if (typeof name !== 'string') {
        console.log('Got argument type', typeof name, ':', name);
        name = 'error: Unexpected argument type ' + (typeof name);
    }
    
    if (name == intereString)
        return interestRate;
    else
        return "Wrong post request";
    return name;

};
